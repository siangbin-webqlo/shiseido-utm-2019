<?php

namespace App\Http\Controllers;

use DB;
use Mail;
use Excel;
use Datatables;
use App\Mail\ThankYouForRegistering;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Carbon\Carbon;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the admin dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard');
    }

    public function datatablesTotalRegistration()
    {
        $total = new Collection;
        $startDate = new Carbon( env('START_TIME') );
        $endDate = new Carbon( env('END_TIME') );

        // Loop through registrations table by date

        $date = $startDate;

        while ( $date->lte($endDate) && !$date->isTomorrow() ) {
            $date_current =$date->format('j F Y (l)');
            $date_from = $date->toDateString();
            $date_to = $date->addDay()->toDateString();
            $total_registrations = DB::table('registrations')->whereBetween( 'created_at', [$date_from,$date_to])->count();
            $total_redeem_massage = DB::table('registrations')->whereBetween( 'updated_at', [$date_from,$date_to])->where('redeemed', 1)->count();
            $total_redeem_ice_cream = DB::table('registrations')->whereBetween( 'updated_ice_cream_at', [$date_from,$date_to])->where('redeemed_ice_cream', 1)->count();
            $total->push([
                'date' => $date_current,
                'total' => $total_registrations,
                'total_massage' => $total_redeem_massage,
                'total_ice_cream' => $total_redeem_ice_cream
            ]);
        }
        $total->reverse();
        return Datatables::of($total)->make(true);
    }

    public function datatablesRegistrationList()
    {
        $registrations = DB::table('registrations')->select([
            'id',
            'fullname',
            'email',
            'phone',
            'unique_code as code',
            'redeem_location as location',
            'created_at as time',
            'updated_at as time_massage',
            'updated_ice_cream_at as time_ice_cream',
        ])->orderBy('created_at', 'desc')->get();

        $registrations->transform(function ($item, $key) {
            $time = new Carbon($item->time);
            $time_massage = is_null($item->time_massage) ? "Yet to redeem." : new Carbon($item->time_massage);
            $time_ice_cream = is_null($item->time_ice_cream) ? "Yet to redeem." : new Carbon($item->time_ice_cream);
            $item->time = $time->format('j F Y (l)');
            $item->time_massage = is_null($item->time_massage) ? $time_massage : $time_massage->format('j F Y (l)');
            $item->time_ice_cream = is_null($item->time_ice_cream) ? $time_ice_cream : $time_ice_cream->format('j F Y (l)');
            return $item;
        });

        return Datatables::of($registrations)->make(true);
    }

    public function resendEmail($code)
    {
        Mail::to( DB::table('registrations')->where('unique_code', $code)->value('email') )->send( new ThankYouForRegistering($code) );

        return 'Email sent. <button onclick="window.close()">Close</button>';
    }

    public function exportRegistrationList()
    {
        $fileName = env('APP_NAME') . ' @ ' . Carbon::now();

        Excel::create($fileName, function ($excel) {
            // left align all rows

            $excel->getDefaultStyle()
                ->getAlignment()
                ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $excel->sheet('registrations', function ($sheet) {
                $rowIndex = 1;

                $sheet->row($rowIndex, [
                    'Full Name',
                    'Register Date',
                    'Email Address',
                    'Phone Number',
                    'Redeem Location',
                    'Redeemed Massage Date',
                    'Redeemed Ice Cream Date',
                ]);

                $registrations = DB::table('registrations')->orderBy('id', 'asc')->get();

                foreach ($registrations as $key => $value) {
                    $rowIndex++;
                    $time = new Carbon($value->created_at);
                    $time_massage = is_null($value->updated_at) ? "Yet to redeem." : new Carbon($value->updated_at);
                    $time_ice_cream = is_null($value->updated_ice_cream_at) ? "Yet to redeem." : new Carbon($value->updated_ice_cream_at);
                    $sheet->row($rowIndex, [
                        $value->fullname,
                        $time->format('j F Y (l)'),
                        $value->email,
                        $value->phone,
                        $value->redeem_location,
                        is_null($value->updated_at) ? $time_massage : $time_massage->format('j F Y (l)'),
                        is_null($value->updated_ice_cream_at) ? $time_ice_cream : $time_ice_cream->format('j F Y (l)'),
                    ]);
                }

                $sheet->setAutoSize(true);
            });
        })->download('xlsx');
    }
    public function exportTotalCount()
    {
        $fileName = env('APP_NAME') . '_total_count @ ' . Carbon::now();

        Excel::create($fileName, function ($excel) {
            // left align all rows

            $excel->getDefaultStyle()
                ->getAlignment()
                ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $excel->sheet('registrations', function ($sheet) {
                $rowIndex = 1;

                $sheet->row($rowIndex, [
                    'Date',
                    'Total Registration',
                    'Total Redeemed Massage',
                    'Total Redeemed Ice Cream',
                ]);

                $total = new Collection;
                $startDate = new Carbon( env('START_TIME') );
                $endDate = new Carbon( env('END_TIME') );
                // Loop through registrations table by date
                $date = $startDate;

                while ( $date->lte($endDate) && !$date->isTomorrow() ) {
                    $date_current =$date->format('j F Y (l)');
                    $date_from = $date->toDateString();
                    $date_to = $date->addDay()->toDateString();
                    $total_registrations = DB::table('registrations')->whereBetween( 'created_at', [$date_from,$date_to])->count();
                    $total_redeem_massage = DB::table('registrations')->whereBetween( 'updated_at', [$date_from,$date_to])->where('redeemed', 1)->count();
                    $total_redeem_ice_cream = DB::table('registrations')->whereBetween( 'updated_ice_cream_at', [$date_from,$date_to])->where('redeemed_ice_cream', 1)->count();
                    $total->push([
                        'date' => $date_current,
                        'total' => $total_registrations,
                        'total_massage' => $total_redeem_massage,
                        'total_ice_cream' => $total_redeem_ice_cream
                    ]);
                }
                foreach ($total as $value) {
                    $rowIndex++;
                    $sheet->row($rowIndex, [
                        $value['date'],
                        $value['total'],
                        $value['total_massage'],
                        $value['total_ice_cream'],
                    ]);
                }

                $sheet->setAutoSize(true);
            });
        })->download('xlsx');
    }
}