<?php

namespace App\Http\Controllers;

use DB;
use Mail;
use Validator;
use App\Mail\ThankYouForRegistering;
use App\Mail\RedeemIceCream;
use Illuminate\Http\Request;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Show the application home.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ( Carbon::parse( env('END_TIME') )->isFuture() ) {
            return view('new_form');
        } else {
            return view('end');
        }
    }

    public function submit(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'fullname' => 'required|max:255',
    		'email' => 'required|email|max:255|unique:registrations,email',
    		'phone' => 'required|regex:/\d+/|max:20|unique:registrations,phone',
            'location' => 'required',
            'tnc' => 'accepted',
        ], [
            'fullname.required' => 'Name is required to participate.',
            'fullname.max' => 'Name is too long.',
            'email.required' => 'Email address is required to participate.',
            'email.email' => 'Email address must be valid.',
            'email.max' => 'Email address is too long.',
            'email.unique' => 'This email address has already been registered.',
            'phone.required' => 'Phone number is required to participate.',
            'phone.regex' => 'Only contains numbers. Plus signs, dashes, and letters are not allowed.',
            'phone.max' => 'Phone number should not exceed 20 characters.',
            'phone.unique' => 'This phone number has already been registered.',
            'location.required' => 'Location must be chosen.',
            'tnc.accepted' => 'Policy must be accepted.',
        ]);

        if ( $validator->fails() ) {
            return response()->json($validator->errors());
        }else{
           // Generate code and create voucher

            $code = $this->generateUniqueCode();

            // Create new registration
            DB::table('registrations')->insert([
                'fullname' => $request->fullname,
                'email' => $request->email,
                'phone' => $request->phone,
                'unique_code' => $code,
                'redeem_location' => $request->location,
                'receive_updates' => $request->receive_update,
                'created_at' => Carbon::now(),
            ]); 
            return response()->json(['success'=>$code]);
        }
    }

    public function thankYou($code)
    {
        $registration = DB::table('registrations')->where('unique_code', $code)->first();
        HomeController::makeVoucher($code);
        // Send email
        Mail::to( $registration->email )->send( new ThankYouForRegistering($code) );

        if ($registration) {
            return view('new-thank-you', [
                'code' => $code,
            ]);
        } else {
            abort(404);
        }
    }

    public function redeem($id,$code){
        $registration = DB::table('registrations')->where([['unique_code', '=', $code],['id','=', $id]])->first();
        $is_valid = 0;
        $type = "shiseido";
        if ( Carbon::parse( env('END_TIME') )->isFuture() ) {
            if( $registration->redeemed !== 0 ){
                $is_valid = 1;
                return view('redeem', ['is_valid' => $is_valid, 'type' => $type]);
            }
            DB::table('registrations')->where('id', $id)->update([
                'redeemed'   => 1,
                'updated_at' => Carbon::now()
            ]);
            Mail::to( $registration->email )->send( new RedeemIceCream($code) );
            return view('redeem',['is_valid' => $is_valid, 'type' => $type]);
        }else{
            $type = "not-available";
            return view('redeem',['is_valid' => $is_valid, 'type' => $type]);
        }
    }

    public function redeemIceCream($id,$code){
        $registration = DB::table('registrations')->where([['unique_code', '=', $code],['id','=', $id]])->first();
        $is_valid = 0;
        $type = "kindkones";
        if ( Carbon::parse( env('END_TIME') )->isFuture() ) {
            if( $registration->redeemed_ice_cream !== 0 ){
                $is_valid = 1;
                return view('redeem', ['is_valid' => $is_valid, 'type' => $type]);
            }
            DB::table('registrations')->where('id', $id)->update([
                'redeemed_ice_cream' => 1,
                'updated_ice_cream_at' => Carbon::now()
            ]);
            return view('redeem',['is_valid' => $is_valid, 'type' => $type]);
        }else{
            $type = "not-available";
            return view('redeem',['is_valid' => $is_valid, 'type' => $type]);
        }
    }

    public function redirect($target)
    {
        DB::table('clicks')->insert([
            'target' => $target,
            'created_at' => Carbon::now(),
        ]);

        switch ($target) {
            case 'facebook':
            case 'facebook-email':
                $url = 'https://www.facebook.com/ShiseidoMalaysia/';
                break;
            case 'youtube':
                $url = 'https://youtu.be/y9TgDVF3XDk';
                break;
            case 'youtube-email':
                $url = 'https://youtu.be/y9TgDVF3XDk';
                break;
            case 'homelink':
                $url = 'https://www.shiseido.com.my/';
                break;
            case 'shiseido-utm-face-product':
                $url = 'https://www.shiseido.com.my/ultimune-power-infusing-concentrate-1011453430.html';
                break;
            case 'shiseido-utm-eye-product':
                $url = 'https://www.shiseido.com.my/ultimune-power-infusing-eye-concentrate-n-1011547830.html';
                break;
            case 'lazada-face-email':
                $url = 'https://c.lazada.com.my/t/c.1p0J?intent=false&fallback=true&url=https%3A%2F%2Fwww.lazada.com.my%2Fproducts%2Fshiseido-ultimune-power-infusing-concentrate-n-50ml-i420659543-s611599794.html';
                break;
            case 'lazada-eye-email':
                $url = 'https://c.lazada.com.my/t/c.1HwI?intent=false&fallback=true&url=https%3A%2F%2Fwww.lazada.com.my%2Fproducts%2Fshiseido-ultimune-eye-power-infusing-eye-concentrate-n-15ml-i521906040-s1015772263.html';
                break;
        }

        return redirect($url);
    }

    public function generateUniqueCode()
    {
        $allowedChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        while (true) {
            $code = '';

            for ($i = 0; $i < 5; $i++) {
                $code .= $allowedChars[mt_rand(0, strlen($allowedChars) - 1)];
            }

            $existed = DB::table('registrations')->where('unique_code', $code)->count();

            if (!$existed) {
                break;
            }
        }

         return $code;
    }
    public static function makeVoucher($code) // image method. $code
    {
        $im = imagecreatefromjpeg( public_path('2.jpg') );
        $registration = DB::table('registrations')->where('unique_code', $code)->first();
        $textbox_1 = imagettfbbox(15, 0, public_path('fonts/READER-BOLD_1.OTF'), $registration->fullname);
        $textbox_2 = imagettfbbox(12, 0, public_path('fonts/Reader-Regular.otf'), $registration->phone);
        $textbox_3 = imagettfbbox(12, 0, public_path('fonts/Reader-Regular.otf'), $registration->redeem_location);
        $x_index_1 = (320/2) - (($textbox_1[2]-$textbox_1[0])/2);
        $x_index_2 = (320/2) - (($textbox_2[2]-$textbox_2[0])/2);
        $x_index_3 = (320/2) - (($textbox_3[2]-$textbox_3[0])/2);
        imagettftext($im, 14, 0, $x_index_1 , 40, imagecolorallocate($im, 113,114,114), public_path('fonts/READER-BOLD_1.OTF'), $registration->fullname);
        imagettftext($im, 12, 0, $x_index_2, 80, imagecolorallocate($im, 113,114,114), public_path('fonts/Reader-Regular.otf'),$registration->phone);
        imagettftext($im, 12, 0, $x_index_3, 120, imagecolorallocate($im, 224,25,50), public_path('fonts/Reader-Regular.otf'),$registration->redeem_location);
        return imagejpeg($im, public_path('voucher/') . $code . '.png');
    }

    public function test($code){
        $registration = DB::table('registrations')->where('unique_code', $code)->first();
        return view('email.thank-you-new',['user' => $registration]);
        // return view('email.redeem-ice-cream',['user' => $registration]);
    }
     public function testEmail() // email method, email screenshot method.
    {
        $email = ['sbfoo@webqlo.com'];
        $code = 'DLLAP';
        $user = DB::table('registrations')->where('unique_code', $code)->first();
        HomeController::makeVoucher($code);
        Mail::to( $email )->send( new ThankYouForRegistering($code) );
        return view('email.thank-you-new',['user'=>$user]);
    }
}
