<?php

namespace App\Mail;

use DB;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ThankYouForRegistering extends Mailable
{
    use Queueable, SerializesModels;
    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($code)
    {
        $this->user = DB::table('registrations')->where('unique_code', $code)->first();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(['Shiseido' => 'no-reply@shiseido.com.my'])
            ->subject('Shiseido Ultimune Sample Kit + Free Massage')
            ->view('email.thank-you-new');
    }
}
