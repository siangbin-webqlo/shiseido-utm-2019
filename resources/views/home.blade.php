@extends('layouts.app')
@push('css')
    <link href="{{ asset('css/animate.min.css') }}" rel="stylesheet" />
@endpush
@section('content')
<div class="container-fluid">
    <a class="floating-logo-btn" href="{{ Carbon\Carbon::parse( env('END_TIME') )->isFuture() ? '#registration_form' : '#end' }}">
        <img class="animated infinite pulse" src="{{ asset('images/utm/floating-icon.png') }}" alt="floating-button">
    </a>
    <div id="top-section">
        @include('header')
        <div class="container step-section mb-50">
            <div class='col-sm-12 p-0 hidden-xs hidden-sm position-relative'>
                <img class='img-responsive' src="{{ asset('images/new/new-bg-desk.jpg') }}" alt="background-desk">
                <div class="floating-title">
                    <img class='image-title' src="{{ asset('images/new/masthead-desk.png') }}" alt="title-image text-center">
                </div>
                <div class="floating-content">
                    <div class="text-center">
                        <h3>Boost your inner strength and<br>pamper yourself with</h3>
                        <h1>Go Go Berry Power!</h1>
                        <p class="pb-25">Inspired by the Ultimune Complex™, we worked with Kind Kones to create this unique flavour. Treat your senses with a thyme infused vanilla, raspberry, and rose ice cream complete with a raspberry rose swirl.</p>
                        <p>You can also receive a quick <strong>facial or hand massage</strong> and a <strong>3-day Ultimune Eye Sample Kit</strong> to boost your skin's outer strength.</p>
                    </div>
                </div>
            </div>
            <div class='col-sm-12 p-0 hidden-lg hidden-md'>
                <img class='img-responsive' src="{{ asset('images/new/new-bg-mob.jpg') }}" alt="background-mob">
                <div class="floating-content">
                    <div class="text-center position-relative full-content">
                        <img class='img-responsive' src="{{ asset('images/new/masthead-mob.png') }}" alt="title-image">
                        <h3>Boost your inner strength and<br>pamper yourself with</h3>
                        <h1>Go Go Berry Power!</h1>
                        <div class="float-bottom">
                            <p>Inspired by the Ultimune Complex™, we worked with Kind Kones to create this unique flavour. Treat your senses with a thyme infused vanilla, raspberry, and rose ice cream complete with a raspberry rose swirl.</p>
                            <p>You can also receive a quick <strong>facial or hand massage</strong> and a <strong>3-day Ultimune Eye Sample Kit</strong> to boost your skin's outer strength.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="four-steps-section">
                <div class="row text-center">
                    <p class="blue-text col-sm-12 pt-25">Follow these steps to redeem your free massage and ice cream!</p>
                    <div class="col-xs-6 col-md-3 custom-min-height">
                        <h1>01</h1>
                        <p>Sign up via the form below and receive your email for a quick facial or hand massage.</p>
                    </div>
                    <div class="col-xs-6 col-md-3 custom-min-height">
                        <h1>02</h1>
                        <p>Visit any Shiseido boutique in Klang Valley to enjoy your massage.</p>
                    </div>
                    <div class="col-xs-6 col-md-3 custom-min-height">
                        <h1>03</h1>
                        <p>After your massage, receive your 3-day Ultimune Eye Sample Kit and email for a complimentary ice cream.</p>
                    </div>
                    <div class="col-xs-6 col-md-3 custom-min-height">
                        <h1>04</h1>
                        <p>Redeem your "Go Go Berry Power!" ice cream from Kind Kones Bangsar or Mid Valley Megamall!</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container form-section">
            @yield('new_form')
        </div>
        <div class='container p-0'>
            <div class='row'>
                <div class='col-sm-12 hidden-xs'>
                    <img class='img-responsive' src="{{ asset('images/utm/banner_desktop.jpg') }}">
                </div>
                <div class='col-sm-12 hidden-lg hidden-md hidden-sm'>
                    <img class='img-responsive' src="{{ asset('images/utm/banner_mobile.jpg') }}">
                </div>
                <div class='col-sm-12 hidden-lg hidden-md hidden-sm'>
                    <img class='img-responsive' src="{{ asset('images/utm/banner2_mobile.jpg') }}">
                </div>
            </div>
        </div>
        <div id="video-section">
            <div class='container'>
                <div class="row">
                    <div class="normal-text text-center">
                       One fierce defender. Continuously bolsters skin’s ability to fortify itself from within. #Ultimune. Made for inner skin defenses. Made with soul. 
                    </div>
                    <h4 class="blue-text text-uppercase">NEW ULTIMUNE POWER INFUSING EYE CONCENTRATE. <br/>POWER INFUSING EYE CONCENTRATE</h4>
                    {{--@if (Carbon\Carbon::parse( env('VIDEO_FIRST_START_TIME') ) > Carbon\Carbon::now())--}}
                    <div class="video-container"> 
                        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/y9TgDVF3XDk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    {{--@else
                        @include('video-carousel')
                    @endif--}}
                </div><!-- row -->
            </div>
        </div>
        <div class='container p-0' id="about_sec">
            <div class='technology-section'>
                <div class='row'>
                    <div class='col-sm-12 col-md-6 position-relative mb-50'>
                        <div class="technology-detail-floating">
                            <h2 class="white-text text-uppercase text-left">REIGNITE SKIN’S BEAUTY FROM WITHIN</h2>
                            <h4 class="white-text text-left">See The Effects Of Friction Reduced By 30%*. Look Forward To 100% Brilliance Around The Eyes.</h4>
                        </div>
                        <div class='technology-image'>
                            <img class='img-responsive mobile-banner' src="{{ asset('images/utm/detail_bg.jpg') }}">
                        </div>
                    </div>
                    <div class='col-sm-12 col-md-6 set-min-height'>
                        <div class="technology-detail">
                            <h2 class="blue-text text-uppercase text-left">technology</h2>
                            <h4 class="blue-text text-uppercase text-left">imugeneration technology<sup>tm</sup></h4>
                            <p class="text-left pb-25">Helps to enhance skin's inner defense system while improving its outward appearance.</p>
                            <h4 class="blue-text text-uppercase text-left">smoothing defense complex</h4>
                            <p class="text-left">Provides a shield-like effect to help reduce friction damage from rubbing and rigorous makeup removal.</p>
                        </div>
                    </div>
                    <div class='col-sm-12 col-md-6 set-min-height position-relative custom-height'>
                        <div class="technology-detail">
                            <h2 class="blue-text text-uppercase text-left">ingredients</h2>
                            <h4 class="blue-text text-uppercase text-left">reishi mushroom extract</h4>
                            <p class="text-left pb-25">An extraordinary mushroom often used in herbal remedies. Revered throughout history for its inherent vital powers of healing.</p>
                            <h4 class="blue-text text-uppercase text-left">iris root extract</h4>
                            <p class="text-left w-75">The iris plant is known for its essential energy and innate strength. Even after a year in the desert, it maintains the ability to sprout new buds and grow.</p>
                            <img class="floating-image" src="{{ asset('images/utm/Ingredient.png') }}">
                        </div>
                    </div>
                    <div class='col-sm-12 col-md-6 set-min-height proven-results'>
                        <div class="technology-detail position-relative">
                            <h2 class="blue-text text-uppercase text-left">proven results</h2>
                            <h4 class="blue-text text-uppercase text-left">effects of friction damage <br>reduced by</h4>
                            <span class="blue-text big-text pb-25">30%<sup class="small-text">*</sup></span>
                            <div class="small">*Extent of friction damage measured as friction properly compared with before application.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="bottom-section">
        <div class="slider-section mb-50 mt-50">
            <div id="product_sec"> 
                <div class='container slider-container'>
                    <h2 class="blue-text mb-50">Shiseido Ultimune Collection</h2>
                    <div class='row'>
                        <div class="col-xs-12 col-md-12 col-centered">
                            <div class='owl-carousel' id='product-carousel'>   
                                <div class="item">
                                    <div class="carousel-col">
                                        <div class="carousel-product">
                                            <img class="carousel-product-img product1" src="{{ asset('images/utm/Product1.png') }}" height="auto" width="auto">
                                        </div>
                                        <div class="carousel-content active">
                                            <div class="carousel-title blue-text">
                                                ULTIMUNE POWER INFUSING CONCENTRATE <br>(RM410)
                                            </div>
                                            <div class="carousel-text">
                                                Helps to boost inner skin defenses and prevent damage. For smooth, radiant, and resilient skin.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="carousel-col">
                                        <div class="carousel-product">
                                            <img class="carousel-product-img" src="{{ asset('images/utm/Product2.png') }}" height="auto" width="auto">
                                        </div>
                                        <div class="carousel-content">
                                            <div class="carousel-title blue-text">
                                                ULTIMUNE POWER INFUSING EYE CONCENTRATE <br>(RM410)
                                            </div>
                                            <div class="carousel-text">
                                                Shields the delicate eye area from the effects of friction. Helps to boost inner skin defenses and prevent damage. For firm, smooth, and radiant results.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>   
        </div> 
        <div id="road_show" class="container road-show">
            <div class="schedule-road-show">
                <h2 class="text-center pb-25 text-uppercase white-text">join ultimune <br>#thefutureproof roadshows</h2>
                <div class="schedule-road-show-detail">
                    <div class="row">
                        <div class="col-12 col-md-6 text-uppercase">
                            <div class="road-show-wrap">
                                <div class="road-show-date text-center">24 JUN<br>-<br>30 JUN</div>
                                <div class="road-show-location">gf center court<br><br>mid valley megamall</div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 text-uppercase">
                            <div class="road-show-wrap">
                                <div class="road-show-date text-center">2 JUL<br>-<br>7 JUL</div>
                                <div class="road-show-location">gf center court<br><br>tebrau city (new wing)</div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 text-uppercase">
                            <div class="road-show-wrap">
                                <div class="road-show-date text-center">15 JUL<br>-<br>21 JUL</div>
                                <div class="road-show-location">level 3, centre court<br><br>pavilion kl</div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 text-uppercase">
                            <div class="road-show-wrap">
                                <div class="road-show-date text-center">29 JUL<br>-<br>4 AUG</div>
                                <div class="road-show-location"><br>gf gurney plaza</div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 text-uppercase">
                            <div class="road-show-wrap">
                                <div class="road-show-date text-center">12 AUG<br>-<br>18 aug</div>
                                <div class="road-show-location">gf center court<br><br>1utama</div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 text-uppercase">
                            <div class="road-show-wrap">
                                <div class="road-show-date text-center">27 AUG<br>-<br>1 sep</div>
                                <div class="road-show-location">gf west court<br><br>aeon bukit indah</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="reward-road-show">
                <h2 class="text-center text-uppercase blue-text mb-50">exclusive rewards @ roadshows</h2>
                <div class="reward-road-show-detail">
                    <div class="row">
                        <div class="col-xs-6 bottom-link">
                            <div class="m-auto custom-max-width">
                                <h4 class="text-left blue-text text-uppercase">powerful eye set @ rm275 <br>ultimune power infusing eye <br>concentrate 15ml</h4>
                                <p><strong>Complimentary</strong></p>
                                <ol>
                                    <li>Deep Cleaning Foam 15ml</li>
                                    <li>Treatment Softener Enriched 75ml</li>
                                    <li>Ultimune Eye Mask</li>
                                </ol>
                            </div>
                        </div>
                        <div class="col-xs-6 pb-25 image-align-center"><img class='img-responsive float-right' src="{{ asset('images/utm/exclusive_1.png') }}"></div>
                        <div class="col-xs-6 bottom-link d-none d-sm-flex">
                            <div class="m-auto custom-max-width">
                                <h4 class="blue-text text-uppercase text-left">pick your own</h4>
                                <p><strong>4-pc gifts & a Deluxe Pouch with any purchase of RM550* & above.</strong></p>
                                <div class="small">*Terms and Conditions apply.</div>
                            </div>
                        </div>
                        <div class="col-xs-6 image-align-center"><img class='img-responsive' src="{{ asset('images/utm/exclusive_shadow.png') }}"></div>
                        <div class="col-xs-6 bottom-link d-flex d-sm-none">
                            <div class="m-auto custom-max-width">
                                <h4 class="blue-text text-uppercase text-left">pick your own</h4>
                                <p><strong>4-pc gifts & a Deluxe Pouch with any purchase of RM550* & above.</strong></p>
                                <div class="small">*Terms and Conditions apply.</div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    @include('footer')
</div>
@endsection