@extends('layouts.app')
@section('content')
@php
	if( isset($is_valid) && isset($type) ){
		if ( $type == "shiseido" ){
			$custom_css = "custom-spacing";
			if( $is_valid == 0 ){
				$bg_image = "images/redeem/shiseido-gift-still-valid.jpg";
				$mob_bg_image = "images/redeem/shiseido-gift-still-valid-mob.jpg";
			}else{
				$bg_image = "images/redeem/shiseido-gift-redeemed.jpg";
				$mob_bg_image = "images/redeem/shiseido-gift-redeemed-mob.jpg";
			
			}
		}elseif ( $type == "not-available" ){
			$bg_image = "images/redeem/not-available-desk.jpg";
			$mob_bg_image = "images/redeem/not-available-mob.jpg";
			$custom_css = "custom-height";
		}else{
			if( $is_valid == 0 ){
				$bg_image = "images/redeem/kk-gift-still-valid.jpg";
				$mob_bg_image = "images/redeem/kk-gift-still-valid-mob.jpg";
			}else{
				$bg_image = "images/redeem/kk-gift-redeemed.jpg";
				$mob_bg_image = "images/redeem/kk-gift-redeemed-mob.jpg";
			}
		}
	}else{
		abort(404);
	}
@endphp
<div id="redeem">
	<div class="desktop-view container-fluid position-relative text-center">
		<img class="img-responsive hidden-xs hidden-sm" src="{{ asset($bg_image) }}" alt="background-desk">
		<img class="img-responsive hidden-lg hidden-md" src="{{ asset($mob_bg_image)}}" alt="background-desk">
		@if ( $type !== "not-available" )
        <div class="text-center {{$custom_css}}">
           @if( $is_valid == 0 && $type == "shiseido" )
           		<p>Redeem your massage and<br>3-day Ultimune Eye Sample Kit now!</p>
           @elseif( $is_valid == 1 && $type == "shiseido" )
           		<p>Hope you're enjoyed your massage!<br>To redeem your ice cream, please check your email.</p>
           @elseif( $is_valid == 0 && $type == "kindkones" )
           		<p>Get your sweet treat now!</p>
           @else
           		<p>The ice cream is gone.<br>Hope you enjoyed your treat!</p>
           @endif
        </div>
        @endif
	</div>
</div>
@endsection