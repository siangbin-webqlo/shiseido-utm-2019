@extends('layouts.admin')
@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading"><strong>Total Registration by Date</strong></div>
            <div class="panel-body">
                <div class="form-group text-right">
                    <a class="btn btn-primary" href="{{ route('export-total-count') }}" target="_blank">Export XLSX</a>
                </div>
                <table id="total_registration_table" class="table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Total Registration</th>
                            <th>Total Redeemed Massage</th>
                            <th>Total Redeemed Ice cream</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- Total Registration by DateTables -->
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        @if ( Auth::user()->username == 'webqlo' )
            <blockquote>
                <strong>End Time: </strong>{{ Carbon\Carbon::parse( env('END_TIME') )->format('j F Y, ga') }}
            </blockquote>

            <div class="panel panel-default">
                <div class="panel-heading"><strong>Total Link Clicks</strong></div>

                <div class="panel-body">
                    <table class="table table-bordered" width="100%">
                        <thead>
                            <tr>
                                <th>Facebook</th>
                                <th>YouTube</th>
                                <th>Home page</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{ DB::table('clicks')->where('target', 'facebook')->count() }}</td>
                                <td>{{ DB::table('clicks')->where('target', 'youtube')->count() }}</td>
                                <td>{{ DB::table('clicks')->where('target', 'homelink')->count() }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><strong>Total Link Clicks (from Email)</strong></div>

                <div class="panel-body">
                    <table class="table table-bordered" width="100%">
                        <thead>
                            <tr>
                                <th>Facebook </th>
                                <th>YouTube</th>
                                <th>Lazada Face Product</th>
                                <th>Lazada Eye Product</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{ DB::table('clicks')->where('target', 'facebook-email')->count() }}</td>
                                <td>{{ DB::table('clicks')->where('target', 'youtube-email')->count() }}</td>
                                <td>{{ DB::table('clicks')->where('target', 'lazada-face')->count() }}</td>
                                <td>{{ DB::table('clicks')->where('target', 'lazada-eye')->count() }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>       
        @endif
        <div class="panel panel-default">
            <div class="panel-heading"><strong>Total Redeemed</strong></div>

            <div class="panel-body">
                <table class="table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th>Total Registration</th>
                            <th>Total Redeemed</th>
                            <th>Total Redeemed Ice Cream</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{ DB::table('registrations')->count() }}</td>
                            <td>{{ DB::table('registrations')->where('redeemed', 1)->count() }}</td>
                            <td>{{ DB::table('registrations')->where('redeemed_ice_cream', 1)->count() }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading"><strong>Registration List</strong></div>

            <div class="panel-body">
                <div class="form-group text-right">
                    <a class="btn btn-primary" href="{{ route('export-registration-list') }}" target="_blank">Export XLSX</a>
                </div>

                <table id="registration_list_table" class="table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th>Full Name</th>
                            <th>Register Date</th>
                            <th>Email Address</th>
                            <th>Phone Number</th>
                            <th>Redeem Location</th>
                            <th>Redeemed Massage Date</th>
                            <th>Redeemed Ice Cream Date</th>
                            @if ( Auth::user()->username == 'webqlo' )
                            <th>Redeem Code</th>
                            <th>Action</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        <!-- Registration List by DateTables -->
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script>
$(function () {
    $('#total_registration_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{ route('datatables-total-registration') }}',
        columns: [
            {data: 'date', name: 'date', orderable: false},
            {data: 'total', name: 'total', orderable: false},
            {data: 'total_massage', name: 'total_massage', orderable: false},
            {data: 'total_ice_cream', name: 'total_ice_cream', orderable: false}
        ]
    });

    $('#registration_list_table').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: '{{ route('datatables-registration-list') }}',
        order: [],
        columns: [
            {
                data: 'fullname'
            },
            {
                data: 'time'
            },
            {
                data: 'email'
            },
            {
                data: 'phone'
            },
            {
                data: 'location'
            },
            {
                render: function(data, type, row, meta) {
                    if( row.time_massage != 'Yet to redeem.' ){
                        return '<span class="label label-success">'+row.time_massage+'</span>';    
                    }
                    return '<span class="label label-danger">'+row.time_massage+'</span>';
                }
            },
            {
                render: function(data, type, row, meta) {
                    if( row.time_ice_cream != 'Yet to redeem.' ){
                        return '<span class="label label-success">'+row.time_ice_cream+'</span>';    
                    }
                    return '<span class="label label-danger">'+row.time_ice_cream+'</span>';
                }
            }
            @if ( Auth::user()->username == 'webqlo' )
            ,
            {
                data: 'code'
            },
            {
                render: function(data, type, row, meta) {
                    return '<a class="btn btn-default btn-sm" href="/admin/resend-email/' + row.code + '" target="_blank">Resend Email</a>';
                }
            }
            @endif
        ]
    });
});
</script>
@endpush
