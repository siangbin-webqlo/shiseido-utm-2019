@extends('layouts.app')
@section('content')
<div class="container-fluid full-height text-center">
    <div id="thank_you" class="container">
        <div class="col-sm-12 text-center custom-padding">
           <img class="image-thankyou pb-25" src="{{ asset('images/new/thankyou_pg.png') }}" alt="thankyou_pg">
           <p>Please check your email to redeem your <strong>massage and <br>a 3-day Ultimune Eye Sample Kit.</strong></p>
           <p>You'll only receive an email to redeem your Kind Kones <br>ice cream after your massage!</p>
        </div>
    </div>
</div>
@endsection
