@extends('layouts.email')
@section('content')
<!--[if (gte mso 9)|(IE)]>
<table border="0" align="center" cellspacing="0" width="600" style="width:600px; max-width: 600px !important;">
  <tr>
    <td>
<![endif]-->
<table border="0" cellspacing="0" align="center" bgcolor="#EEE0DD" style="background-color:#EEE0DD;width:100%; max-width: 600px !important;">
    <tr>
        <td style="text-align: center;">
             <img src="{{ asset('images/new-edm/top_banner_edm_1.jpg') }}" alt="Logo" style="width:100%;max-width: 100%;"/>
        </td>
    </tr>
    <tr align="center">
        <td style="text-align: center; padding: 0 15px !important; position: relative;">
            <a href="{{ env('APP_URL') }}/redeem-sample-kit/{{ $user->id }}/{{ $user->unique_code }}" target="_blank">
              <img src="{{ asset('voucher/'.$user->unique_code.'.png') }}" alt="table" style="width:100%;max-width: 100%;"/>
            </a>
        </td>
    </tr>
    <tr align="center">
        <td style="padding: 0 15px 15px !important;">
           <table border="0" cellpadding="0">
               <tr align="center">
                   <td align="center" style="width: 50%; padding: 0 15px;">
                        <a href="{{ route('redirect', ['target' => 'shiseido-utm-face-product']) }}" target="_blank">
                          <img src="{{ asset('images/new-edm/product_1.png') }}" alt="product eye" />
                        </a>
                   </td>
                   <td align="center" style="width: 50%; padding: 0 15px;">
                        <a href="{{ route('redirect', ['target' => 'shiseido-utm-eye-product']) }}" target="_blank">
                            <img src="{{ asset('images/new-edm/product_2.png') }}" alt="product eye" />
                        </a>
                   </td>
               </tr>
               <tr align="center">
                 <td align="center" style="width: 50%; padding: 0 15px;">
                        <a href="{{ route('redirect', ['target' => 'lazada-face-email']) }}" target="_blank">
                          <img src="{{ asset('images/new-edm/shop-now-button.png') }}" alt="product eye" />
                        </a>
                 </td>
                 <td align="center" style="width: 50%; padding: 0 15px;">
                      <a href="{{ route('redirect', ['target' => 'lazada-eye-email']) }}" target="_blank">
                        <img src="{{ asset('images/new-edm/shop-now-button.png') }}" alt="product eye" />
                      </a>
                 </td>
               </tr>
           </table>
        </td>
    </tr>
    <tr align="center"><td><img src="{{ asset('images/edm/line.png') }}" alt="line" /></td></tr>
    <tr>
        <td style="padding: 0 15px;">
           <table border="0" cellpadding="0">
               <tr>
                   <td align="left" width="500">
                       <p style="font-size: 0.7em;"><a href="{{ env('APP_URL') }}" target="_blank" style="color: #717272;">Terms & Conditions</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="{{ env('APP_URL') }}" target="_blank" style="color: #717272;">Privacy Policy</a><br/>Copyright &#9400;2019 Shiseido Co. Ltd. All rights reserved.</p>
                   </td>
                   <td align="right" width="100">
                      <a href="{{ route('redirect', ['target' => 'facebook-email']) }}" target="_blank"><img src="{{ asset('images/edm/fb-icon.png') }}" alt="button" /></a>
                       &nbsp;&nbsp;&nbsp;&nbsp;
                      <a href="{{ route('redirect', ['target' => 'youtube-email']) }}" target="_blank"><img src="{{ asset('images/edm/YT-icon.png') }}" alt="button"/></a>
                   </td>
               </tr>
           </table> 
        </td>
    </tr>
</table>
<!--[if (gte mso 9)|(IE)]>
    </td>
  </tr>
</table>
<![endif]-->
@endsection