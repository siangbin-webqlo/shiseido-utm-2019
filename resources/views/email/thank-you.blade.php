@extends('layouts.email')
@section('content')
<table>
    <tr>
        <td style="text-align: center;">
             <img src="{{ asset('images/utm/shiseido-logo.png') }}" alt="Logo" style="width:300px; margin-bottom: 50px;" />
        </td>
    </tr>
    <tr>
        <td>
            <p>Dear {{ $user->fullname }},</p>

            <h1>Thank you for your interest!</h1>

            <p>
                You may redeem this exclusive <strong style="color: #e01932;">Shiseido Ultimune Sample Kit</strong> by presenting this verification code <strong style="color: #e01932;">{{ $user->unique_code }}</strong> and your identity card at your preferred Shiseido store location at <br />
                <strong style="color: #e01932;">{{ $user->redeem_location }}</strong>
            </p>

            <h4>Following is the info you provided to us:</h4>

            <p>
                <ul>
                    <li><strong>Your Name:</strong> {{ $user->fullname }}</li>
                    <li><strong>Email address:</strong> {{ $user->email }}</li>
                    <li><strong>Phone number:</strong> {{ $user->phone }}</li>
                </ul>
            </p>            
        </td>
    </tr>
    <tr align="center" style="padding-bottom: 25px;">
        <td>
            <p style="text-align: left;"><strong>Show this email to any friendly Shiseido team at store location mentioned above.</strong></p>
            <p><a href="{{ env('APP_URL') }}/redeem-sample-kit/{{ $user->id }}/{{ $user->unique_code }}" target="_blank" style="color: #fff; background-color:#e01932; padding: 10px 15px; text-transform: uppercase; text-decoration: none;">
                <strong>click to redeem</strong>
            </a></p>
        </td>
    </tr>
    <tr>
        <td>
            <p>Thank you for your participation, have a lovely day!</p>
            <p>
                <a href="{{ route('redirect', ['target' => 'facebook-email']) }}" target="_blank" style="color: #e01932;">Follow Shiseido on Facebook</a>
            </p>
            <p>
                Terms & Conditions : 
                <br/>
                    The exclusive Shiseido Ultimune Sample Kit is while stocks last only.
                <br/>
                    One redemption per customer please.
                <br/>
                    Duplicate redemptions will not be entertained
                <br/>
                The campaign will run from {{ Carbon\Carbon::parse( env('START_TIME') )->format('j F Y, g:i:sa') }} until {{ Carbon\Carbon::parse( env('END_TIME') )->format('j F Y, g:i:sa') }}.
            </p>
        </td>
    </tr>
</table>
@endsection
