 <nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" href="/" rel="home page"><img class="nav-logo" src="{{ asset('images/utm/shiseido-logo.png') }}" height="auto" width="auto"></a>
        </div>
        <div class="navbar-container container">
            <ul class="nav navbar-nav">
              <li><a href="#about_sec"><span>About</span></a></li>
              <li><a href="#product_sec"><span>Products</span></a></li>
              <li><a href="{{ Carbon\Carbon::parse( env('END_TIME') )->isFuture() ? '#registration_form' : '#end' }}"><span>Free Sample</span></a></li>
              {{--<li><a href="#road_show"><span>Roadshows</span></a></li>--}}
            </ul>
        </div>
    </div>
</nav>