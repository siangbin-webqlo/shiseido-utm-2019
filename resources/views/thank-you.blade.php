@extends('layouts.app')
@section('content')
<div class="container-fluid full-height text-center">
    <div id="thank_you" class="container">
        <a href="/"><img class="img-responsive center-block thank-you-logo" src="{{ asset('images/utm/thank-you-logo.png') }}" alt="Shiseido Icon Logo" /></a>
        <h2 class="mt-25">
            Thank you for your details!
        </h2>

        <h2 class="dear-name mt-25">
            Dear {{ DB::table('registrations')->where('unique_code', $code)->first()->fullname }},
        </h2>
        <p class="mt-25">
            You may redeem this exclusive <strong class="blue-text">Shiseido Ultimune Sample Kit</strong> by presenting this verification code <strong class="blue-text">{{ $code }}</strong> and your identity card at your preferred Shiseido store location at <br />
            <strong class="blue-text">{{ DB::table('registrations')->where('unique_code', $code)->first()->redeem_location }}</strong>
        </p>
        <p class="mt-50">
            Share the beauty with your friends.
        </p>
        <div class="mt-25 mb-50 bottom-link">
            <a id="logo-btn" class="btn-share" href="{{ route('redirect', ['target' => 'homelink']) }}" target="_blank"><img src="{{ asset('/images/utm/footer-logo-white.png') }}" width="auto" height="auto" class="m-auto thank-you-bottom-logo"/></a>
            <a class="btn-share" href="{{ route('redirect', ['target' => 'facebook']) }}" target="_blank"><i class="m-auto fa fa-facebook-square fa-2x"></i></a>
            <a class="btn-share" href="{{ route('redirect', ['target' => 'youtube']) }}" target="_blank"><i class="m-auto fa fa-youtube-square fa-2x"></i></a>
        </div>
    </div>
</div>
@endsection
