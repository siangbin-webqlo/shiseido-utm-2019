<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    @if ( env('LIVE') == 1)
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-89145400-2"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-89145400-2');
    </script>
    @endif
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Tab icon -->
    <link rel="icon" href="{{ asset('images/utm/footer-logo.png') }}">
    <meta name="robots" content="noindex">
    <meta name="googlebot" content="noindex">
    <meta property="og:url"           content="{{ env('APP_URL') }}" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="SHISEIDO ULTIMUNE" />
    <meta property="og:description"   content="One fierce defender. Continuously bolsters skin’s ability to fortify itself from within. #Ultimune. Made for inner skin defenses. Made with soul." />
    <meta property="og:image"         content="{{ asset('images/utm/banner_desktop.jpg') }}" />
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Styles -->
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ asset('plugin/OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugin/OwlCarousel2-2.3.4/dist/assets/owl.theme.default.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/sunsquad.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
    <link href="{{ asset('css/animate.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/new_style.css?ver=3.1') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        @yield('content')
    </div>
    <!-- Scripts -->
    <script src="//tt.mbww.com/tt-b914d613c73fdf0799fee7084089c159b41636d61254f83a347f2c651eb1efa0.js" async></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
    <script defer src="{{ asset('plugin/OwlCarousel2-2.3.4/dist/owl.carousel.js') }}"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="{{ asset('js/system.js?ver=3.0') }}"></script> 
    @stack('js')
</body>
</html>
