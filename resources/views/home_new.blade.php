@extends('layouts.app')
@section('content')
<div class="container-fluid utm-bg">
    <a class="floating-logo-btn" href="#registration_form">
        <img class="animated infinite pulse" src="{{ asset('images/utm/floating-icon.png') }}" alt="floating-button">
    </a>
    <div id="top-section">
        @include('header')
        <div class='container p-0'>
            <div class='row'>
                <div class='col-sm-12 hidden-xs'>
                    <img class='img-responsive' src="{{ asset('images/utm/banner_desktop.jpg') }}">
                </div>
                <div class='col-sm-12 hidden-lg hidden-md hidden-sm'>
                    <img class='img-responsive' src="{{ asset('images/utm/banner_mobile.jpg') }}">
                </div>
                <div class='col-sm-12 hidden-lg hidden-md hidden-sm'>
                    <img class='img-responsive' src="{{ asset('images/utm/banner2_mobile.jpg') }}">
                </div>
            </div>
        </div>
        <div id="video-section">
            <div class='container'>
                <div class="row">
                    <div class="normal-text text-center">
                       One fierce defender. Continuously bolsters skin’s ability to fortify itself from within. #Ultimune. Made for inner skin defenses. Made with soul. 
                    </div>
                    <h4 class="blue-text text-uppercase">NEW ULTIMUNE POWER INFUSING EYE CONCENTRATE. <br/> POWER INFUSING EYE CONCENTRATE</h4>
                    <div class="video-container"> 
                        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/_1nFanR4WwA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        {{-- @include('video-carousel') --}}
                    </div>
                </div><!-- row -->
            </div>
        </div>
        <div class='container p-0'>
            <div class='technology-section'>
                <div class='row'>
                    <div class='col-sm-12 col-md-6 position-relative mb-50'>
                        <div class="technology-detail-floating">
                            <h2 class="white-text text-uppercase text-left">REIGNITE SKIN’S BEAUTY FROM WITHIN</h2>
                            <p class="white-text text-left">See The Effects Of Friction Reduced By 30%*. Look Forward To 100% Brilliance Around The Eyes.</p>
                        </div>
                        <div class='technology-image'>
                            <img class='img-responsive mobile-banner' src="{{ asset('images/utm/detail_bg.jpg') }}">
                        </div>
                    </div>
                    <div class='col-sm-12 col-md-6'>
                        <div class="technology-detail">
                            <h2 class="blue-text text-uppercase text-left">wetforce technology</h2>
                            <p class="blue-text text-left">Exclusive WetForce Technology binds with minerals found in water or perspiration, adding power to the protective UV veil. The more you sweat, the stronger the protective UV veil.</p>
                        </div>
                        <div class='technology-image'>
                            <img class='img-responsive mobile-banner' src="{{ asset('images/gsc/Wetforce.png') }}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class='container p-0'>
            <div class='proven-result-section'>
                <div class='row'>
                    <div class="col-xs-12">
                        <h2 class="blue-text lucent-title text-uppercase text-left">proven results</h2>
                    </div>
                     <div class='col-md-6 col-sm-12'>
                        <h4 class="blue-text lucent-title text-uppercase text-left">NEW SPORTS BB COMPACT</h4>
                        <img class='img-responsive mobile-banner' src="{{ asset('images/gsc/new_sports_bb.png') }}">
                        <div class="small">*Tested by 100 Japanese women in September 2017 when playing sports on a sunny day.</div>
                    </div>
                    <div class='col-md-6 col-sm-12 pt-md-40'>
                        <h4 class="blue-text lucent-title text-uppercase text-left">SPORTS BB</h4>
                        <img class='img-responsive mobile-banner' src="{{ asset('images/gsc/sports_bb.png') }}">
                        <div class="small">*Tested by 100 Japanese women in September 2017 when playing sports on a sunny day.</div>
                    </div>
                </div>
            </div><!-- proven-result-section -->
        </div>
    </div>
    <div id="bottom-section">
        <div class="slider-section">
            <div id="product-sec"> 
                <div class='container slider-container'>
                    <h2 class="blue-text">BB For Sports And HydroBB Compact For Sports</h2>
                    <div class='row'>
                        <div class="col-xs-12 col-md-12 col-centered">
                            <div class='owl-carousel' id='product-carousel'>   
                                <div class="item">
                                    <div class="carousel-col">
                                        <div class="carousel-product">
                                            <img class="carousel-product-img product1" src="{{ asset('images/gsc/Product1.png') }}" height="auto" width="auto">
                                        </div>
                                        <div class="carousel-content">
                                            <div class="carousel-title blue-text">
                                                NEW SPORTS BB COMPACT (RM120)<br>
                                                <span class="black-text">SPF50+</span>
                                            </div>
                                            <div class="carousel-text">
                                                An emulsion compact that instantly refreshes skin – anytime, anywhere.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="carousel-col">
                                        <div class="carousel-product">
                                            <img class="carousel-product-img" src="{{ asset('images/gsc/Product2.png') }}" height="auto" width="auto">
                                        </div>
                                        <div class="carousel-content active">
                                            <div class="carousel-title blue-text">
                                                SPORTS BB (RM170)<br>
                                                <span class="black-text">SPF50+</span>
                                            </div>
                                            <div class="carousel-text">
                                                A quick-dry BB that provides non-sticky, longer lasting protection.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>   
        </div> 
        <div class="container form-section">
            @yield('form')
        </div>
    </div>
    @include('footer')
</div>
@endsection
@push('js')
<script src="{{ asset('js/smooth-scroll.js?ver=1.0') }}"></script>
@endpush