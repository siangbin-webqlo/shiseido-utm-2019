@extends('home')
@section('new_form')
<div id="end" class="text-center mb-50">
    <p class="blue-text">
        The free sample event has ended on {{ Carbon\Carbon::parse( env('END_TIME') )->format('j F Y, ga') }}
    </p>
    <p>
        Thank you for your participation. Please stay tuned to our upcoming events.
    </p>
</div>
@endsection
