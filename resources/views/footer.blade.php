<div class="page-footer">
    <div class="container">
        <div class="row">
            <div class="footer-links-subsection col-xs-5">
            <ul>
                <li>
                    Follow Us:
                    <a class="footer-social-media" target="_blank" href="{{ route('redirect', ['target' => 'homelink']) }}"><img class="footer-logo-img" src="/images/utm/footer-logo-white.png" height="auto" width="auto"/></a>
                    <a class="footer-social-media" target="_blank" href="{{ route('redirect', ['target' => 'facebook']) }}"><i class="fa fa-facebook"></i></a>
                    <a class="footer-social-media" target="_blank" href="{{ route('redirect', ['target' => 'youtube']) }}"><i class="fa fa-youtube-play"></i></a>
                </li>
            </ul>
            </div>
            <div class="footer-copyright col-xs-7">Copyright by Shiseido Malaysia Sdn Bhd (685030-U).</div>
        </div>
    </div>
</div>