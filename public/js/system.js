$(document).ready(function(){
    var iOS = ( navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false );
    $('.modal').on('hidden.bs.modal', function (e) {
        $('body').css('padding-right',0);
    });
    var owl = $(".owl-carousel");
    owl.owlCarousel({
        items:1,
        loop:true,
        autoheight:false,
        dots : false,
        nav:true
    });
    /*var error = $(".help-block.label.label-danger");
    if( error.length ){
        var target = $("#registration_form");
        if (iOS) {
            document.getElementById('registration_form').scrollIntoView();
        }else{
            $('html, body').animate({
                scrollTop: target.offset().top
            }, 1000);
        }
    }*/
    $("a.footer-social-media img").hover(
      function() {
        $( this ).attr('src','/images/utm/footer-logo.png');
      }, function() {
        $( this ).attr('src','/images/utm/footer-logo-white.png');
      }
    );
    $("a#logo-btn").click(function() {
        $( "a#logo-btn img" ).attr('src','/images/utm/thank-you-logo.png');
    });
    $("a#logo-btn").hover(
      function() {
        $( "a#logo-btn img" ).attr('src','/images/utm/thank-you-logo.png');
      }, function() {
        $( "a#logo-btn img" ).attr('src','/images/utm/footer-logo-white.png');
      }
    );
});